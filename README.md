<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

# Message Scheduler - Laravel
## Flow
![Schedule Message Flow](ScheduleMessageFlow.png?raw=true "Schedule Message Flow")

## Required Files
| **File**                         | **Path**              | **Description**                                                                                    |
| -------------------------------- | --------------------- | -------------------------------------------------------------------------------------------------- |
| ScheduleMessageQueueJobAudit.php | app/Console/Commands/ | Console command to create queue jobs for "active" schedule_command without queue jobs              |
| Kernel.php                       | app/Console/          | Registration of Schedule task `ScheduleMessageQueueJobAudit`                                       |
| QueueScheduleMessage.php         | app/Events/           | Custom event for dispatching a new schedule_message queue job                                      |
| ProcessMessage.php               | app/Jobs/             | Should be replaced with the job responsible for sending out messages                               |
| ProcessScheduleMessage.php       | app/Jobs              | Queue definition for schedule messages                                                             |
| QueueScheduleMessageListener.php | app/Listeners/        | Listener for event QueueScheduleMessage                                                            |
| ScheduleMessage.php              | app/Models/           | ScheduleMessage model. All model methods required                                                  |
| ScheduleMessageQueueItem.php     | app/Models/           | Custom model to be used as payload for schedule message queue                                      |
| EventServiceProvider.php         | app/Providers/        | Event `QueueScheduleMessage` and listener `QueueScheduleMessageListener` should be registered here |
| ScheduleMessageService.php       | app/Services/         | Class responsible for managing some operations for the message scheduling flow                     |
| MessageService.php               | app/Services/         | Should be replaced with the service responsible for managing message processing                    |

## Details
- Minute and Hour should use leading 0 format.

## Installation
1. Download and install Docker with Docker Compose
2. Clone the project
3. Execute the script below in the project root to run a docker container containing PHP and Composer to install the application's dependencies at the appropriate PHP version (PHP 8.1):
    ```bash
    docker run --rm \
    -u "$(id -u):$(id -g)" \
    -v $(pwd):/opt \
    -w /opt \
    laravelsail/php81-composer:latest \
    composer install --ignore-platform-reqs
    ```
4. Make a copy of the .env.sail.example:
    ``` bash
    cp .env.sail.example .env
    ```
5. Modify the docker host ports used in the `docker-compose.yml` file in the .env if there are any clashes with your local docker port binding.
    1. APP_PORT        | default: 80
    2. FORWARD_DB_PORT | default: 3306

6. Start the app
    ``` bash
    ./vendor/bin/sail up -d
    ```

7. Generate APP_KEY
    ``` bash
    ./vendor/bin/sail artisan key:generate
    ```
   
8. Run Migrations
    ``` bash
    ./vendor/bin/sail artisan migrate
    ```

## Running Tests
### Unit Tests
This project has a few Unit tests.
Run the tests with the command below:
``` bash
./vendor/bin/sail test --group unitTest
```

### Functional Tests
connect to the database and create the test database
```sql
CREATE DATABASE `message_scheduler_laravel_test` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
```

provide all permissions to user `sail` on database `message_scheduler_laravel_test`  (except GRANT ALL)

Run the tests with the command below:
``` bash
./vendor/bin/sail test --group functionalTest
```

### Simulator
It is possible to simulate the scheduling of messages to test the flow for `specific_date`, `daily`, `weekly` and `monthly` schedule types.
To simulate a schedule message follow the steps below:

1. Open the terminal and run the command below:
    ```bash
    ./vendor/bin/sail artisan queue:listen --queue=scheduled-message-queue,process-message-queue
    ```
    **note:** the container must have been started before running the command above

2. Open a new terminal and run the command following the prompts:
    ```bash
    ./vendor/bin/sail artisan schedule_message:simulator
    ```
   
3. When the time comes, you should notice the queue consumer pick up the job and process it for both queues `scheduled-message-queue` and `process-message-queue`. You can validate the simulated message sent by checking the logs in `app/storage/logs/mock-message-processed.log`
   1. for `daily` schedule type, the job will be set to go out the next day. You can validate this by running the following in a Tinker session:
      ```bash
      unserialize(json_decode(DB::table('jobs')->find({queue id})->payload, true)['data']['command']);
      ```
      **note:** Substitute the `queue id` for the appropriate queue id in the jobs table.
   2. Validate the `delay` property has the expected DateTime
