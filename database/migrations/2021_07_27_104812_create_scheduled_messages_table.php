<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateScheduledMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('scheduled_messages', function (Blueprint $table) {
            $table->id();
            $table->string('campaign_id')->nullable();
            $table->string('company_id')->nullable();
            $table->string('message_id')->nullable();
            $table->string('title')->nullable();
            $table->string('author')->nullable();
            $table->string('group_id');
            $table->string('schedule_date')->nullable()->comment('date to send the message');
            $table->string('schedule_type')->nullable()->comment('specific dates,Daily, weekly, monthly');
            $table->boolean('is_recurring')->default(0)->comment('true, false');
            $table->string('frequency')->nullable()->comment('Eg. Every 2 weeks (14), Every 2 months (28)');
            $table->string('minute')->nullable();
            $table->string('hour')->nullable();
            $table->string('specific_days')->nullable()->comment('num of days => 1-7');
            $table->string('specific_week_days')->nullable()->comment('list of days => Monday, Tuesday, Wednesday');
            $table->string('specific_month_days')->nullable()->comment('List of days => 1 - 30');
            $table->string('recurring_option_monthly')->nullable()->comment('Eg. First week');
            $table->text('additional_recipients')->nullable()->comment('hold extra recipients numbers added during scheduling');
            $table->string('timezone')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('scheduled_messages');
    }
}
