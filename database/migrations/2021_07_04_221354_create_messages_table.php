<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->id();
            $table->string('company_id');
            $table->string('title')->nullable();
            $table->text('message');
            $table->string('category')->nullable()->comment('adhoc,schedule');
            $table->string('status')->nullable();
            $table->string('author')->nullable();
            $table->string('message_type')->comment('sms, voice');
            $table->string('size')->nullable();
            $table->string('format')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
