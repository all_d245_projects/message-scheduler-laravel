<?php

namespace Tests\TestCases;

use App\Jobs\ProcessMessage;
use App\Jobs\ProcessScheduledMessage;
use App\Models\Campaign;
use App\Models\ScheduleMessage;
use Illuminate\Support\Facades\DB;
use DateTime;

class ScheduleMessageTestCase extends LaravelTestCase
{
    public function setUp(): void
    {
        parent::setUp();
    }

    /**
     * Returns a bare instance of a ScheduleMessage
     *
     * @param array $config
     * @return ScheduleMessage
     */
    protected function get_bare_schedule_message_instance(array $config): ScheduleMessage
    {
        return new ScheduleMessage([
            'message_id' => $config['message_id'] ?? null,
            'campaign_id' => $config['campaign_id'] ?? null,
            'schedule_date' => $config['schedule_date'] ?? null,
            'schedule_type' => $config['schedule_type'],
            'is_recurring' => $config['is_recurring'],
            'frequency' => $config['frequency'] ?? null,
            'minute' => $config['minute'],
            'hour' => $config['hour'],
            'specific_week_days' => $config['specific_week_days'] ?? null,
            'specific_month_days' => $config['specific_month_days'] ?? null,
            'timezone' => $config['timezone'] ?? null,
        ]);
    }

    /**
     * Returns an instance of a ScheduleMessage for a campaign
     *
     * @param array $config
     * @return ScheduleMessage
     */
    protected function get_campaign_schedule_message_instance(array $config): ScheduleMessage
    {
        return new ScheduleMessage([
            'message_id' => $config['message_id'] ?? null,
            'campaign_id' => $config['campaign_id'] ?? null,
            'schedule_date' => $config['schedule_date'] ?? null,
            'schedule_type' => $config['schedule_type'],
            'is_recurring' => $config['is_recurring'],
            'frequency' => $config['frequency'],
            'minute' => $config['minute'],
            'hour' => $config['hour'],
            'specific_week_days' => $config['specific_week_days'] ?? null,
            'specific_month_days' => $config['specific_month_days'] ?? null,
            'timezone' => $config['timezone'] ?? null,
        ]);
    }

    /**
     * Returns a Campaign instance
     *
     * @param string $status
     * @param string $start_date
     * @param string $end_date
     * @return Campaign
     */
    protected function get_campaign_instance(string $status, string $start_date, string $end_date): Campaign
    {
        return new Campaign([
            'status' => $status,
            'start_date' => $start_date,
            'end_date' => $end_date
        ]);
    }

    /**
     * @return bool
     */
    protected function schedule_message_job_table_empty(): bool
    {
        $result = DB::table('jobs')
            ->where('queue', '=', 'scheduled-message-queue')
            ->get();

        return $result->count() === 0;
    }

    /**
     * @param int $schedule_message_id
     * @param DateTime $delay
     * @return bool
     */
    protected function schedule_message_delay_valid(int $schedule_message_id, DateTime $delay)
    {
        $found_and_validated = false;

        $result = DB::table('jobs')
            ->where('queue', '=', 'scheduled-message-queue')
            ->get();

        foreach ($result->all() as $row) {
            $payload = json_decode($row->payload, true);
            $data = $payload['data']['command'];

            $data = unserialize($data);

            /** @var ProcessScheduledMessage $data */
            if ($data->uniqueId() == $schedule_message_id && $delay == $data->delay) {
                $found_and_validated = true;
                break;
            }
        }

        return $found_and_validated;
    }
}
