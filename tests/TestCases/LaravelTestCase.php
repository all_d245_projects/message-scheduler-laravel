<?php

namespace Tests\TestCases;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Tests\Helpers\CreatesApplication;

abstract class LaravelTestCase extends BaseTestCase
{
    use CreatesApplication;
}
