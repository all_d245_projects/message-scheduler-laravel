<?php

namespace Tests\Feature\Models\ScheduleMessage;

use App\Models\ScheduleMessage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCases\ScheduleMessageTestCase;
use DateTime;

class SpecificDateScheduleMessageTest extends ScheduleMessageTestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();
        $this->createApplication();
    }

    /**
     * @group functionalTest
     */
    public function test_specific_date_scheduled_message_saved_with_no_message_does_not_queue()
    {
        $attributes = $this->get_default_schedule_message_attributes();
        $attributes['schedule_date'] = (new DateTime('+1 week'))->format('Y-m-d');
        $attributes['schedule_type'] = ScheduleMessage::SCHEDULE_TYPE_SPECIFIC;
        $attributes['is_recurring'] = false;

        ScheduleMessage::create($attributes);

        $this->assertTrue($this->schedule_message_job_table_empty());
    }

    /**
     * @group functionalTest
     */
    public function test_specific_date_scheduled_message_saved_with_message_will_queue()
    {
        $attributes = $this->get_default_schedule_message_attributes();
        $attributes['message_id'] = 1;
        $attributes['schedule_date'] = (new DateTime('+1 week'))->format('Y-m-d');
        $attributes['schedule_type'] = ScheduleMessage::SCHEDULE_TYPE_SPECIFIC;
        $attributes['is_recurring'] = false;

        $schedule_message = new ScheduleMessage($attributes);
        $schedule_message->save();

        $this->assertTrue(
            $this->schedule_message_delay_valid($schedule_message->id, $schedule_message->get_next_execution())
        );
    }

    /**
     * @return array
     */
    private function get_default_schedule_message_attributes(): array
    {
        return [
            'title' => 'some title',
            'group_id' => 0,
            'hour' => '12',
            'minute' => '30'
        ];
    }
}
