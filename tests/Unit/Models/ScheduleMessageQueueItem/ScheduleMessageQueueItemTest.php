<?php

namespace Tests\Unit\Models\ScheduleMessageQueueItem;

use App\Models\ScheduleMessageQueueItem;
use PHPUnit\Framework\TestCase;
use DateTime;

class ScheduleMessageQueueItemTest extends TestCase
{
    /**
     * @group unitTest
     */
    public function test_model_created_ok()
    {
        $dateTime = new DateTime('2021-12-03');
        $id = 'some-id-string';

        $model = new ScheduleMessageQueueItem($id, $dateTime);

        $this->assertEquals($id, $model->get_schedule_message_id());
        $this->assertEquals($dateTime, $model->get_next_execution());
        $this->assertEquals(0, $model->get_rescheduled());
        $this->assertEquals(0, $model->get_failed());
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_update_ok()
    {
        $dateTime = new DateTime('2021-12-03');
        $id = 'some-id-string';

        $model = new ScheduleMessageQueueItem($id, $dateTime);

        $updated_dateTime = new DateTime('2021-12-05');
        $model->set_next_execution($updated_dateTime);

        $this->assertEquals($updated_dateTime, $model->get_next_execution());
        $this->assertTrue($model->get_executions()->contains($dateTime));
        $this->assertTrue($model->get_executions()->contains($updated_dateTime));
        $this->assertEquals(1, $model->get_rescheduled());
    }

}
