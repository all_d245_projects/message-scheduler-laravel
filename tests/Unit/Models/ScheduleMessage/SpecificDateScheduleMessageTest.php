<?php

namespace Tests\Unit\Models\ScheduleMessage;

use App\Models\ScheduleMessage;
use DateTime;
use Tests\TestCases\ScheduleMessageTestCase;

class SpecificDateScheduleMessageTest extends ScheduleMessageTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->createApplication();
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_specific_date_in_the_future()
    {
        $simulated_current_date = new DateTime('2021-01-01');

        $schedule_message_config = [
            'schedule_date' => '2021-02-01',
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_SPECIFIC,
            'is_recurring' => false,
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        $this->assertEquals(
            '2021-02-01 15:00',
            $next_execution->format('Y-m-d H:i')
        );
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_specific_date_in_the_past()
    {
        $simulated_current_date = new DateTime('2021-03-01');

        $schedule_message_config = [
            'schedule_date' => '2021-02-01',
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_SPECIFIC,
            'is_recurring' => false,
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        $this->assertNull($next_execution);
    }

    /**
     * @group unitTest
     */
    public function test_message_will_reoccur_for_specific_date()
    {
        $simulated_current_date = new DateTime('2021-01-01');

        $schedule_message_config = [
            'message_id' => 1,
            'schedule_date' => '2021-02-01',
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_SPECIFIC,
            'is_recurring' => false,
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $this->assertTrue($schedule_message->will_message_reoccur());
    }

    /**
     * @group unitTest
     */
    public function test_message_will_not_reoccur_for_specific_date()
    {
        $simulated_current_date = new DateTime('2021-03-01');

        $schedule_message_config = [
            'message_id' => 1,
            'schedule_date' => '2021-02-01',
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_SPECIFIC,
            'is_recurring' => false,
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $this->assertFalse($schedule_message->will_message_reoccur());
    }
}
