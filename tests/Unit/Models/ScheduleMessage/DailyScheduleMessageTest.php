<?php

namespace Tests\Unit\Models\ScheduleMessage;

use App\Models\ScheduleMessage;
use DateTime;
use Tests\TestCases\ScheduleMessageTestCase;

class DailyScheduleMessageTest extends ScheduleMessageTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->createApplication();
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_daily_frequency_of_1()
    {
        $simulated_current_date = new DateTime('2021-01-01');

        $schedule_message_config = [
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_DAILY,
            'is_recurring' => true,
            'frequency' => 1,
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        $this->assertEquals(
            '2021-01-01 15:00',
            $next_execution->format('Y-m-d H:i')
        );
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_daily_frequency_of_1_same_day_occurrence()
    {
        $simulated_current_date = new DateTime('2021-01-01 15:00:01');

        $schedule_message_config = [
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_DAILY,
            'is_recurring' => true,
            'frequency' => 1,
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        $this->assertEquals(
            '2021-01-02 15:00',
            $next_execution->format('Y-m-d H:i')
        );
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_daily_frequency_of_2()
    {
        $simulated_current_date = new DateTime('2021-01-01');

        $schedule_message_config = [
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_DAILY,
            'is_recurring' => true,
            'frequency' => 2,
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        $this->assertEquals(
            '2021-01-03 15:00',
            $next_execution->format('Y-m-d H:i')
        );
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_daily_frequency_of_3()
    {
        $simulated_current_date = new DateTime('2021-01-01');

        $schedule_message_config = [
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_DAILY,
            'is_recurring' => true,
            'frequency' => 3,
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        $this->assertEquals(
            '2021-01-04 15:00',
            $next_execution->format('Y-m-d H:i')
        );
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_daily_frequency_of_5_rolls_over_at_the_end_of_the_month()
    {
        $simulated_current_date = new DateTime('2021-01-28');

        $schedule_message_config = [
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_DAILY,
            'is_recurring' => true,
            'frequency' => 5,
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        $this->assertEquals(
            '2021-02-02 15:00',
            $next_execution->format('Y-m-d H:i')
        );
    }

    /**
     * @group unitTest
     */
    public function test_message_will_reoccur_for_daily()
    {
        $simulated_current_date = new DateTime('2021-01-28');

        $schedule_message_config = [
            'message_id' => 1,
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_DAILY,
            'is_recurring' => true,
            'frequency' => 5,
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $this->assertTrue($schedule_message->will_message_reoccur());
    }
}
