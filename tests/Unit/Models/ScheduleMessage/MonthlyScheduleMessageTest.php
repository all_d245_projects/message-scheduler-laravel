<?php

namespace Tests\Unit\Models\ScheduleMessage;

use App\Models\ScheduleMessage;
use DateTime;
use Tests\TestCases\ScheduleMessageTestCase;

class MonthlyScheduleMessageTest extends ScheduleMessageTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->createApplication();
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_monthly_frequency_of_1()
    {
        /**
         * First simulated date
         */
        $simulated_current_date = new DateTime('2021-01-01');

        $schedule_message_config = [
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_MONTHLY,
            'is_recurring' => true,
            'frequency' => 1,
            'specific_month_days' => '1,2,3',
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be 2021-01-02 from the
         * simulated current date of 2021-01-01
         */
        $this->assertEquals(
            '2021-01-02 15:00',
            $next_execution->format('Y-m-d H:i')
        );

        /**
         * Next simulated date
         */
        $simulated_current_date = new DateTime('2021-01-02');
        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be 2021-01-03 from the
         * simulated current date of 2021-01-02
         */
        $this->assertEquals(
            '2021-01-03 15:00',
            $next_execution->format('Y-m-d H:i')
        );

        /**
         * Next simulated date
         */
        $simulated_current_date = new DateTime('2021-01-03');
        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be 2021-02-01 from the
         * simulated current date of 2021-01-03
         */
        $this->assertEquals(
            '2021-02-01 15:00',
            $next_execution->format('Y-m-d H:i')
        );
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_monthly_frequency_of_1_unordered_month_days()
    {
        /**
         * First simulated date
         */
        $simulated_current_date = new DateTime('2021-01-12');

        $schedule_message_config = [
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_MONTHLY,
            'is_recurring' => true,
            'frequency' => 1,
            'specific_month_days' => '6,12,2',
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be 2021-02-02 from the
         * simulated current date of 2021-01-12
         */
        $this->assertEquals(
            '2021-02-02 15:00',
            $next_execution->format('Y-m-d H:i')
        );

        /**
         * Next simulated date
         */
        $simulated_current_date = new DateTime('2021-02-02');
        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be 2021-02-06 from the
         * simulated current date of 2021-02-02
         */
        $this->assertEquals(
            '2021-02-06 15:00',
            $next_execution->format('Y-m-d H:i')
        );

        /**
         * Next simulated date
         */
        $simulated_current_date = new DateTime('2021-02-06');
        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be 2021-02-12 from the
         * simulated current date of 2021-02-06
         */
        $this->assertEquals(
            '2021-02-12 15:00',
            $next_execution->format('Y-m-d H:i')
        );
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_monthly_frequency_of_1_rolls_over_at_the_end_of_the_month()
    {
        /**
         * First simulated date
         */
        $simulated_current_date = new DateTime('2021-08-30');

        $schedule_message_config = [
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_MONTHLY,
            'is_recurring' => true,
            'frequency' => 1,
            'specific_month_days' => '31,1,30',
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be 2021-08-31 from the
         * simulated current date of 2021-08-30
         */
        $this->assertEquals(
            '2021-08-31 15:00',
            $next_execution->format('Y-m-d H:i')
        );

        /**
         * Next simulated date
         */
        $simulated_current_date = new DateTime('2021-08-31');
        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be 2021-09-01 from the
         * simulated current date of 2021-08-31
         */
        $this->assertEquals(
            '2021-09-01 15:00',
            $next_execution->format('Y-m-d H:i')
        );

        /**
         * Next simulated date
         */
        $simulated_current_date = new DateTime('2021-09-01');
        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be 2021-09-30 from the
         * simulated current date of 2021-09-01
         */
        $this->assertEquals(
            '2021-09-30 15:00',
            $next_execution->format('Y-m-d H:i')
        );
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_monthly_frequency_of_1_rolls_over_at_the_end_of_the_month_with_lesser_days()
    {
       // todo
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_monthly_frequency_of_2()
    {
        /**
         * First simulated date
         */
        $simulated_current_date = new DateTime('2021-01-02');

        $schedule_message_config = [
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_MONTHLY,
            'is_recurring' => true,
            'frequency' => 2,
            'specific_month_days' => '1,2,3',
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be 2021-01-03 from the
         * simulated current date of 2021-01-02
         */
        $this->assertEquals(
            '2021-01-03 15:00',
            $next_execution->format('Y-m-d H:i')
        );

        /**
         * Next simulated date
         */
        $simulated_current_date = new DateTime('2021-01-03');
        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be 2021-03-01 from the
         * simulated current date of 2021-01-03
         */
        $this->assertEquals(
            '2021-03-01 15:00',
            $next_execution->format('Y-m-d H:i')
        );

        /**
         * Next simulated date
         */
        $simulated_current_date = new DateTime('2021-03-01');
        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be 2021-03-02 from the
         * simulated current date of 2021-03-01
         */
        $this->assertEquals(
            '2021-03-02 15:00',
            $next_execution->format('Y-m-d H:i')
        );
    }

    /**
     * @group unitTest
     */
    public function test_message_will_reoccur_for_monthly()
    {
        $simulated_current_date = new DateTime('2021-01-01');

        $schedule_message_config = [
            'message_id' => 1,
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_MONTHLY,
            'is_recurring' => true,
            'frequency' => 1,
            'specific_month_days' => '1,3,5',
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $this->assertTrue($schedule_message->will_message_reoccur());
    }
}
