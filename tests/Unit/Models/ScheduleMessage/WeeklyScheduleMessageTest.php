<?php

namespace Tests\Unit\Models\ScheduleMessage;

use App\Models\ScheduleMessage;
use DateTime;
use Tests\TestCases\ScheduleMessageTestCase;

class WeeklyScheduleMessageTest extends ScheduleMessageTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->createApplication();
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_weekly_frequency_of_1()
    {
        /**
         * Simulated date on a Friday: init
         */
        $simulated_current_date = new DateTime('2021-01-01');

        $schedule_message_config = [
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_WEEKLY,
            'is_recurring' => true,
            'frequency' => 1,
            'specific_week_days' => 'Tuesday,Wednesday',
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be Tuesday (2021-01-05) from the
         * simulated current date of Friday (2021-01-01)
         */
        $this->assertEquals(
            '2021-01-05 15:00',
            $next_execution->format('Y-m-d H:i')
        );

//        /**
//         * Simulated date on Tuesday: first specific_week_days
//         */
//        $simulated_current_date = new DateTime('2021-01-05');
//        $schedule_message->set_test_datetime($simulated_current_date);
//
//        $next_execution = $schedule_message->get_next_execution();
//
//        /**
//         * We expect the next execution date to be Wednesday (2021-01-06) from the
//         * simulated current date of Tuesday (2021-01-05)
//         */
//        $this->assertEquals(
//            '2021-01-06 15:00',
//            $next_execution->format('Y-m-d H:i')
//        );
//
//        /**
//         * Simulated date on Wednesday: second specific_week_days
//         */
//        $simulated_current_date = new DateTime('2021-01-06');
//        $schedule_message->set_test_datetime($simulated_current_date);
//
//        $next_execution = $schedule_message->get_next_execution();
//
//        /**
//         * We expect the next execution date to be Tuesday (2021-01-12) from the
//         * simulated current date of Wednesday (2021-01-06)
//         */
//        $this->assertEquals(
//            '2021-01-12 15:00',
//            $next_execution->format('Y-m-d H:i')
//        );
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_weekly_frequency_of_1_same_day_occurrence()
    {
        /**
         * Simulated date on a Friday: init
         */
        $simulated_current_date = new DateTime('2021-01-01 12:00');

        $schedule_message_config = [
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_WEEKLY,
            'is_recurring' => true,
            'frequency' => 1,
            'specific_week_days' => 'Friday,Saturday,Sunday',
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution(true);

        /**
         * We expect the next execution date to be Friday (2021-01-01) from the
         * simulated current date of Friday (2021-01-01)
         */
        $this->assertEquals(
            '2021-01-01 15:00',
            $next_execution->format('Y-m-d H:i')
        );

        /**
         * Simulated date on Friday: first specific_week_days time after scheduled time
         */
        $simulated_current_date = new DateTime('2021-01-01 15:00:01');
        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be Saturday (2021-01-02) from the
         * simulated current date of Friday (2021-01-01)
         */
        $this->assertEquals(
            '2021-01-02 15:00',
            $next_execution->format('Y-m-d H:i')
        );

        /**
         * Simulated date on Wednesday: second specific_week_days
         */
        $simulated_current_date = new DateTime('2021-01-02 15:00:01');
        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be Sunday (2021-01-03) from the
         * simulated current date of Saturday (2021-01-06)
         */
        $this->assertEquals(
            '2021-01-03 15:00',
            $next_execution->format('Y-m-d H:i')
        );
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_weekly_frequency_of_1_with_unordered_weekdays()
    {
        /**
         * Simulated date on a Wednesday: init
         */
        $simulated_current_date = new DateTime('2021-01-06 15:00:01');

        $schedule_message_config = [
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_WEEKLY,
            'is_recurring' => true,
            'frequency' => 1,
            'specific_week_days' => 'Friday,Wednesday ,Tuesday',
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be Friday (2021-01-08) from the
         * simulated current date of Wednesday (2021-01-06)
         */
        $this->assertEquals(
            '2021-01-08 15:00',
            $next_execution->format('Y-m-d H:i')
        );

        /**
         * Simulated date on Friday: second specific_week_days (if ordered)
         */
        $simulated_current_date = new DateTime('2021-01-08 15:00:01');
        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be Tuesday (2021-01-12) from the
         * simulated current date of Wednesday (2021-01-08)
         */
        $this->assertEquals(
            '2021-01-12 15:00',
            $next_execution->format('Y-m-d H:i')
        );

        /**
         * Simulated date on Tuesday: first specific_week_days (if ordered)
         */
        $simulated_current_date = new DateTime('2021-01-12 15:00:01');
        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be Wednesday (2021-01-13) from the
         * simulated current date of Tuesday (2021-01-12)
         */
        $this->assertEquals(
            '2021-01-13 15:00',
            $next_execution->format('Y-m-d H:i')
        );
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_weekly_frequency_of_1_rolls_over_at_the_end_of_the_month()
    {
        /**
         * Simulated date on a Sunday: init
         */
        $simulated_current_date = new DateTime('2021-01-31 15:00:01');

        $schedule_message_config = [
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_WEEKLY,
            'is_recurring' => true,
            'frequency' => 1,
            'specific_week_days' => 'Monday, Sunday',
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be Monday (2021-02-01) from the
         * simulated current date of Sunday (2021-01-31)
         */
        $this->assertEquals(
            '2021-02-01 15:00',
            $next_execution->format('Y-m-d H:i')
        );

        /**
         * Simulated date on Monday: first specific_week_days
         */
        $simulated_current_date = new DateTime('2021-02-01 15:00:01');
        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be Sunday (2021-02-07) from the
         * simulated current date of Monday (2021-02-01)
         */
        $this->assertEquals(
            '2021-02-07 15:00',
            $next_execution->format('Y-m-d H:i')
        );
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_weekly_frequency_of_2()
    {
        /**
         * Simulated date on a Friday: init
         */
        $simulated_current_date = new DateTime('2021-01-01 15:00:01');

        $schedule_message_config = [
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_WEEKLY,
            'is_recurring' => true,
            'frequency' => 2,
            'specific_week_days' => 'Tuesday,Wednesday',
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be Tuesday (2021-01-05) from the
         * simulated current date of Friday (2021-01-05)
         */
        $this->assertEquals(
            '2021-01-05 15:00',
            $next_execution->format('Y-m-d H:i')
        );

        /**
         * Simulated date on Tuesday: first specific_week_days
         */
        $simulated_current_date = new DateTime('2021-01-05 15:00:01');
        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be Wednesday (2021-01-06) from the
         * simulated current date of Tuesday (2021-01-05)
         */
        $this->assertEquals(
            '2021-01-06 15:00',
            $next_execution->format('Y-m-d H:i')
        );

        /**
         * Simulated date on Wednesday: second specific_week_days
         */
        $simulated_current_date = new DateTime('2021-01-06 15:00:01');
        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be Tuesday (2021-01-19) from the
         * simulated current date of Wednesday (2021-01-06) as a result of +2 weeks frequency
         */
        $this->assertEquals(
            '2021-01-19 15:00',
            $next_execution->format('Y-m-d H:i')
        );
    }

    /**
     * @group unitTest
     */
    public function test_next_execution_weekly_frequency_of_2_rolls_over_at_the_end_of_the_month()
    {
        /**
         * Simulated date on a Sunday: init
         */
        $simulated_current_date = new DateTime('2021-01-31');

        $schedule_message_config = [
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_WEEKLY,
            'is_recurring' => true,
            'frequency' => 2,
            'specific_week_days' => 'Monday,Sunday',
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $next_execution = $schedule_message->get_next_execution();

        /**
         * We expect the next execution date to be Monday (2021-02-08) from the
         * simulated current date of Sunday (2021-01-31)
         */
        $this->assertEquals(
            '2021-02-08 15:00',
            $next_execution->format('Y-m-d H:i')
        );
    }

    /**
     * @group unitTest
     */
    public function test_message_will_reoccur_for_weekly()
    {
        $simulated_current_date = new DateTime('2021-01-01');

        $schedule_message_config = [
            'message_id' => 1,
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_WEEKLY,
            'is_recurring' => true,
            'frequency' => 1,
            'specific_week_days' => 'Tuesday,Wednesday',
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message->set_test_datetime($simulated_current_date);

        $this->assertTrue($schedule_message->will_message_reoccur());
    }
}
