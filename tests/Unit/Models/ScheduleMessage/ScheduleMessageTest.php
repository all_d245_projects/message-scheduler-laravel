<?php

namespace Tests\Unit\Models\ScheduleMessage;

use App\Models\ScheduleMessage;
use DateTime;
use Tests\TestCases\ScheduleMessageTestCase;
use DateTimeZone;

class ScheduleMessageTest extends ScheduleMessageTestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->createApplication();
    }

    /**
     * @group unitTest
     */
    public function test_get_date_time_zone_will_return_timezone_of_app_if_no_timezone_set()
    {
        $schedule_message_config = [
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_DAILY,
            'is_recurring' => true,
            'frequency' => 1,
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message_timezone = $schedule_message->get_date_time_zone();

        $this->assertInstanceOf(DateTimeZone::class, $schedule_message_timezone);

        $this->assertEquals(
            config('app.timezone'),
            $schedule_message_timezone->getName()
        );
    }

    /**
     * @group unitTest
     */
    public function test_get_date_time_zone_will_return_timezone_set_in_model()
    {
        $schedule_message_config = [
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_DAILY,
            'is_recurring' => true,
            'frequency' => 1,
            'hour' => '15',
            'minute' => '00',
            'timezone' => 'Africa/Accra'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $schedule_message_timezone = $schedule_message->get_date_time_zone();

        $this->assertInstanceOf(DateTimeZone::class, $schedule_message_timezone);

        $this->assertEquals(
            'Africa/Accra',
            $schedule_message_timezone->getName()
        );
    }

    /**
     * @group unitTest
     */
    public function test_message_will_not_reoccur_because_there_is_no_message_linked()
    {
        $schedule_message_config = [
            'schedule_date' => '2021-02-01',
            'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_SPECIFIC,
            'is_recurring' => false,
            'hour' => '15',
            'minute' => '00'
        ];

        $schedule_message = $this->get_bare_schedule_message_instance($schedule_message_config);

        $this->assertFalse($schedule_message->will_message_reoccur());
    }
}
