<?php

namespace App\Jobs;

use App\Models\ScheduleMessageQueueItem;
use App\Services\ScheduledMessageService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUniqueUntilProcessing;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Exception;
use Throwable;

class ProcessScheduledMessage implements ShouldQueue, ShouldBeUniqueUntilProcessing
{
    use Dispatchable, InteractsWithQueue, Queueable;

    /**
     * The number of times the job may be attempted.
     *
     * @var int
     */
    public $tries = 5;

    /**
     * @var ScheduleMessageQueueItem
     */
    protected $queue_item;

    /**
     * Create a new job instance.
     *
     * @param ScheduleMessageQueueItem $queue_item
     */
    public function __construct(ScheduleMessageQueueItem $queue_item)
    {
        $this->queue = 'scheduled-message-queue';
        $this->queue_item = $queue_item;
    }

    /**
     * The unique ID of the job.
     *
     * @return string
     */
    public function uniqueId()
    {
        return (string) $this->queue_item->get_schedule_message_id();
    }

    /**
     * Execute the job.
     *
     * @param ScheduledMessageService $service_handler
     * @return void
     * @throws Exception
     */
    public function handle(ScheduledMessageService $service_handler)
    {
        $schedule_message = $service_handler->get_scheduled_message($this->queue_item);

        if ($schedule_message && $schedule_message->should_message_be_processed()) {
            $service_handler->process_job($this->queue_item, $schedule_message);
        }
    }

    /**
     * Handle a job failure.
     *
     * @param  Throwable  $exception
     * @return void
     */
    public function failed(Throwable $exception)
    {
        // todo maybe consider dispatching job again to be retried the next day or prepare next schedule
    }
}
