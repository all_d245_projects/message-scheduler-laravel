<?php

namespace App\Models;

//use App\Traits\Uuids;
//use App\Transformers\CampaignTransformer;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Message;
use App\Models\ScheduleMessage;

class Campaign extends Model
{
    use SoftDeletes;
//    use Uuids;

    const ACTIVE = 'active';
    const INACTIVE = 'inactive';

    protected $dates = ['deleted_at'];

    protected $fillable = [
        'name',
        'company_id',
        'author',
        'description',
        'message_type',
        'message',
        'status',
        'start_date',
        'end_date',
    ];

    protected $hidden = [
        'pivot'
    ];

//    public $transformer = CampaignTransformer::class;


    public function is_active()
    {
        return $this->status = Campaign::ACTIVE;
    }

    /**
     *  The messages that belongs to the campaign.
     */
    public function messages()
    {
        return $this->belongsToMany(Message::class);
    }

    public function schedules()
    {
        return $this->hasMany(ScheduleMessage::class);
    }
}
