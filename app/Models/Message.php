<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use App\Traits\Uuids;
use App\Models\Campaign;
use App\Models\ScheduleMessage;
//use App\MessageHistory;
//use App\Transformers\MessageTransformer;

class Message extends Model
{
//    use Uuids;

    protected $fillable = [
        'title',
        'company_id',
        'author',
        'message_type',
        'message',
        'size',
        'format',

    ];

    protected $hidden = [
        'pivot'
    ];

//    public $transformer = MessageTransformer::class;


    /**
     *  The campaigns that belongs to the Message.
     */
    public function campaigns()
    {
        return $this->belongsToMany(Campaign::class);
    }

    public function schedule_messages()
    {
        return $this->hasMany(ScheduleMessage::class);
    }

//    public function message_histories()
//    {
//        return $this->hasMany(MessageHistory::class);
//    }
}
