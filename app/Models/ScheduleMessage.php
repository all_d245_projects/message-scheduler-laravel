<?php

namespace App\Models;

//use App\Traits\Uuids;
//use App\Transformers\ScheduleMessageTransformer;
use App\Events\QueueScheduleMessage;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Campaign;
use App\Models\Message;
//use App\Group;
//use App\Contact;
use DateTime;
use Exception;
use DateTimeZone;

class ScheduleMessage extends Model
{
    public const SCHEDULE_TYPE_SPECIFIC = 'specific_date';
    public const SCHEDULE_TYPE_DAILY = 'daily';
    public const SCHEDULE_TYPE_WEEKLY = 'weekly';
    public const SCHEDULE_TYPE_MONTHLY = 'monthly';


//    use Uuids;

    protected $table = 'scheduled_messages';

//    public $transformer = ScheduleMessageTransformer::class;

    protected $fillable = [
        'campaign_id',
        'company_id',
        'author',
        'message_id',
        'title',
        'group_id',
        'schedule_date',
        'schedule_type',
        'specific_days',
        'specific_week_days',
        'recurring_option_monthly',
        'specific_month_days',
        'is_recurring',
        'frequency',
        'minute',
        'hour',
        'additional_recipients',
        'timezone' // I added this to support timezones
    ];

    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'saved' => QueueScheduleMessage::class
    ];

    public function campaign()
    {
        return $this->belongsTo(Campaign::class);
    }

    public function message()
    {
        return $this->belongsTo(Message::class);
    }
//
//    public function group()
//    {
//        return $this->belongsTo(Group::class);
//    }

    /**
     * @param DateTime $dateTime
     * @return void
     */
    public function set_test_datetime(DateTime $dateTime): void
    {
        $this->test_datetime = $dateTime;
    }

    /**
     * @return DateTimeZone
     */
    public function get_date_time_zone(): ?DateTimeZone
    {
       return ($this->timezone)
           ? new DateTimeZone($this->timezone)
           : new DateTimeZone(config('app.timezone'));
    }

    /**
     * @return bool
     */
    public function has_message(): bool
    {
        return $this->message_id !== null;
    }

    /**
     * @return bool
     */
    public function has_campaign(): bool
    {
        return $this->campaign_id !== null;
    }

    /**
     * @return DateTime|null
     * @throws Exception
     */
    public function get_next_execution(): ?DateTime
    {
        $current_date_time = $this->get_current_date();
        $next_date_time = clone $current_date_time;
        $next_date_time->setTime($this->hour, $this->minute);


        switch ($this->schedule_type) {
            case self::SCHEDULE_TYPE_SPECIFIC:
                $next_date_time = $this->get_next_execution_for_specific_date($next_date_time);
                break;
            case self::SCHEDULE_TYPE_DAILY:
                $next_date_time = $this->get_next_execution_for_daily($current_date_time, $next_date_time);
                break;
            case self::SCHEDULE_TYPE_WEEKLY:
                $next_date_time = $this->get_next_execution_for_weekly($current_date_time, $next_date_time);
                break;
            case self::SCHEDULE_TYPE_MONTHLY:
                $next_date_time = $this->get_next_execution_for_monthly($current_date_time, $next_date_time);
                break;
            default:
                throw new Exception('could not create DateTime from invalid schedule type');
        }

        if ($next_date_time) {
            $next_date_time->setTime($this->hour, $this->minute);
        }

        return $next_date_time;
    }

    /**
     * @return bool
     * @throws Exception
     */
    public function will_message_reoccur(): bool
    {
        if (!$this->has_message()) {
            return false;
        }

        $tz = $this->get_date_time_zone();
        $now = $this->get_current_date();

        if ($this->has_campaign()) {
            $campaign = $this->campaign();

            if ($campaign->status === Campaign::INACTIVE || $now > new DateTime($campaign->end_date, $tz)) {
                return false;
            }

            return $this->get_scheduling_status($now);
        }

        return $this->get_scheduling_status($now);
    }

    /**
     * @return bool
     */
    public function should_message_be_processed(): bool
    {
        // todo
        // message not more than 3 hours old
        // campaign still active
        // campaign not expired
        return true;
    }

    /**
     * @param DateTime $now
     * @return bool
     */
    private function get_scheduling_status(DateTime $now)
    {
        switch ($this->schedule_type) {
            case self::SCHEDULE_TYPE_SPECIFIC:
                return
                    DateTime::createFromFormat(
                        "Y-m-d H:i",
                        sprintf("%s %s:%s", $this->schedule_date, $this->hour, $this->minute)
                    ) > $now;
            case self::SCHEDULE_TYPE_DAILY:
            case self::SCHEDULE_TYPE_WEEKLY:
            case self::SCHEDULE_TYPE_MONTHLY:
                return true;

            default:
                return false;
        }
    }

    /**
     * @param DateTime $date
     * @return DateTime
     * @throws Exception
     */
    private function get_next_execution_for_specific_date(DateTime $date): ?DateTime
    {
        $scheduleDate = DateTime::createFromFormat("Y-m-d", $this->schedule_date);

        if (!$scheduleDate) {
            throw new Exception('Could not create DateTime from schedule_date');
        }

        if (!$this->get_scheduling_status($date)) {
            return null;
        }

        $date->setDate(
            $scheduleDate->format('Y'),
            $scheduleDate->format('m'),
            $scheduleDate->format('d')
        );

        return $date;
    }

    /**
     * @param DateTime $current_date_time
     * @param DateTime $next_date_time
     * @return DateTime
     * @throws Exception
     */
    private function get_next_execution_for_daily(DateTime $current_date_time, DateTime $next_date_time): DateTime
    {
        if (!$this->frequency) {
            throw new Exception('could not create DateTime from schedule_type daily. frequency not valid');
        }

        if ($this->frequency > 1 || $current_date_time > $next_date_time) {
            $next_date_time->modify(sprintf("+%s days", $this->frequency));
        }

        return $next_date_time;
    }

    /**
     * @param DateTime $current_date_time
     * @param DateTime $next_date_time
     * @return DateTime
     * @throws Exception
     */
    private function get_next_execution_for_weekly(DateTime $current_date_time, DateTime $next_date_time): DateTime
    {
        if (!$this->specific_week_days) {
            throw new Exception('Could not create DateTime from schedule_type weekly. specific_week_days not valid');
        }

        if (!$this->frequency) {
            throw new Exception('could not create DateTime from schedule_type weekly. frequency not valid');
        }


        // order list of days
        $ordered_week_days = $this->sort_weekly_days($this->specific_week_days);
        $ordered_week_days = array_map('strtolower', $ordered_week_days);

        $current_week_day_int = strtolower($current_date_time->format('N'));

        foreach ($ordered_week_days as $key => $ordered_week_day) {
            $ordered_week_day_int = date('N', strtotime($ordered_week_day));

            if ($ordered_week_day_int == $current_week_day_int && $this->frequency == 1) {
                if ($next_date_time > $current_date_time) {
                    return $next_date_time;
                }
            } else if ($ordered_week_day_int > $current_week_day_int) {
                $next_date_time->modify(sprintf("next %s", $ordered_week_day));

                return $next_date_time;
            } else if ($current_week_day_int == date('N', strtotime(end($ordered_week_days))))  {
                // we have processed the last week day in the array...apply the frequency and set the first element of the array as the date.

                if ($this->frequency > 1) { // we must ignore frequency of 1 as this sets the next week instead of the intended current week
                    $next_date_time->modify(sprintf("+%s weeks", $this->frequency -1));
                }

                $next_date_time->modify(sprintf("next %s", $ordered_week_days[0]));

                return $next_date_time;
            } else if ($ordered_week_day == end($ordered_week_days)) {
                $next_date_time->modify(sprintf("next %s", $ordered_week_days[0]));

                return $next_date_time;
            }
        }

        throw new Exception('could not create DateTime from schedule_type weekly. Something went wrong in determining the next scheduled day in the ordered_week_days.');
    }

    /**
     * todo: if month day does not exist in current month...set last day of the month
     *
     * @param DateTime $current_date_time
     * @param DateTime $next_date_time
     * @return DateTime
     * @throws Exception
     */
    private function get_next_execution_for_monthly(DateTime $current_date_time, DateTime $next_date_time): DateTime
    {
        if (!$this->specific_month_days) {
            throw new Exception('could not create DateTime from schedule_type monthly. specific_month_days not valid');
        }

        if (!$this->frequency) {
            throw new Exception('could not create DateTime from schedule_type monthly. frequency not valid');
        }

        // order month of days
        $ordered_month_days = $this->sort_monthly_days($this->specific_month_days);

        $ordered_month_days = array_map(function($str) {
            return ltrim($str, '0');
        }, $ordered_month_days);

        return $this->get_next_monthly_datetime($current_date_time, $next_date_time,  $ordered_month_days);
    }

    /**
     * @param string $week_days
     * @return array
     */
    private function sort_weekly_days(string $week_days): array
    {
        $week_order = array_flip(['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']);

        $week_days = array_map('trim', explode(',', $week_days));

        $week_days = array_flip($week_days);

        uksort($week_days, function ($a, $b) use ($week_order) {
            return $week_order[$a] - $week_order[$b];
        });

        return array_keys($week_days);
    }

    /**
     * method modified to re-index
     *
     * @param string $days
     * @return array
     */
    private function sort_monthly_days(string $days): array
    {
        $month_days = array_map('trim', explode(',', $days));

        natsort($month_days);

        return array_values($month_days);
    }

    /**
     * @param DateTime $current_date_time
     * @param DateTime $next_date_time
     * @param array $month_days
     * @param int $iteration
     * @return DateTime
     * @throws Exception
     */
    private function get_next_monthly_datetime(DateTime $current_date_time, DateTime $next_date_time,  array $month_days, int $iteration = 0): DateTime
    {
        if (empty($month_days)) {
            throw new Exception('$month_days cannot be an empty array');
        }

        $current_date_month = $current_date_time->format('j');

        if ($current_date_month == end($month_days)) {
            // rest month day
            $next_date_time->setDate(
                $next_date_time->format('Y'),
                $next_date_time->format('m'),
                1
            );

            $next_date_time->modify(sprintf("+%s months", $this->frequency));

            // todo $this->get_appropriate_date_for_monthly()
            $next_date_time->setDate(
                $next_date_time->format('Y'),
                $next_date_time->format('m'),
                $month_days[0]
            );

            return $next_date_time;
        }

        $next_date_time->modify('+1 days');

        $modified_month_day = $next_date_time->format('j');


        // todo $this->get_appropriate_date_for_monthly()
        if ($modified_month_day > $current_date_time->format('j') &&  in_array($modified_month_day, $month_days)) {
            return $next_date_time;
        }

        if ($iteration > 31) {
            throw new Exception('Too many recursive operations occurred. Aborting.');
        }

        return $this->get_next_monthly_datetime($current_date_time, $next_date_time, $month_days, $iteration++);
    }

    /**
     * Method to allow setting a custom current date time used for testing
     *
     * @return DateTime
     * @throws Exception
     */
    private function get_current_date(): DateTime
    {
        if (config('app.env') === 'testing') {
            if (isset($this->test_datetime)) {
                return $this->test_datetime;
            }
        }

        return new DateTime('now', $this->get_date_time_zone());
    }

    /**
     * @return int
     */
    private function get_appropriate_date_for_monthly(): int
    {
        //todo
    }
}
