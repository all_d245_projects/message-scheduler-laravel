<?php

namespace App\Models;

use DateTime;
use Illuminate\Support\Collection;

class ScheduleMessageQueueItem
{
    /**
     * @var string
     */
    private $schedule_message_id;

    /**
     * @var DateTime
     */
    private $next_execution;

    /**
     * @var Collection
     */
    private $executions;

    /**
     * @var int
     */
    private $failed;

    /**
     * @var int
     */
    private $rescheduled;

    /**
     * @param string $schedule_message_id
     * @param DateTime $next_execution
     */
    public function __construct(string $schedule_message_id, DateTime $next_execution)
    {
        $this->schedule_message_id = $schedule_message_id;
        $this->executions = new Collection();
        $this->failed = 0;
        $this->rescheduled = 0;

        $this->next_execution = $next_execution;
        $this->executions->add($next_execution);
    }

    /**
     * @return string
     */
    public function get_schedule_message_id(): string
    {
        return $this->schedule_message_id;
    }

    /**
     * @return DateTime
     */
    public function get_next_execution(): DateTime
    {
        return $this->next_execution;
    }

    /**
     * @param DateTime $next_execution
     * @return self
     */
    public function set_next_execution(DateTime $next_execution): self
    {
        $this->next_execution = $next_execution;
        $this->executions->add($next_execution);
        $this->rescheduled();
        return $this;
    }

    /**
     * @return Collection
     */
    public function get_executions(): Collection
    {
        return $this->executions;
    }

    /**
     * @return bool
     */
    public function get_failed(): bool
    {
        return $this->failed;
    }

    /**
     * @return bool
     */
    public function get_rescheduled(): bool
    {
        return $this->rescheduled;
    }

    /**
     * @return void
     */
    public function failed(): void
    {
        $this->failed ++;
    }

    /**
     * @return void
     */
    private function rescheduled(): void
    {
        $this->rescheduled ++;
    }
}
