<?php

namespace App\Events;

use App\Models\ScheduleMessage;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class QueueScheduleMessage
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var ScheduleMessage
     */
    public $schedule_message;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(ScheduleMessage $schedule_message)
    {
        $this->schedule_message = $schedule_message;
    }
}
