<?php

namespace App\Services;

use App\Jobs\ProcessScheduledMessage;
use App\Models\ScheduleMessage;
use App\Models\ScheduleMessageQueueItem;
use Exception;
use DateTime;

class ScheduledMessageService
{
    /**
     * @var MessageService
     */
    private $message_service;

    /**
     * @param  MessageService $message_service
     */
    public function __construct(MessageService $message_service)
    {
        $this->message_service = $message_service;
    }

    /**
     * @param ScheduleMessageQueueItem $queue_item
     * @return ScheduleMessage|null
     */
    public function get_scheduled_message(ScheduleMessageQueueItem $queue_item): ?ScheduleMessage
    {
        return ScheduleMessage::find($queue_item->get_schedule_message_id());
    }

    /**
     * @param ScheduleMessage $schedule_message
     * @throws Exception
     */
    public function dispatch_scheduled_message_queue_job(ScheduleMessage $schedule_message)
    {
        $queue_item = new ScheduleMessageQueueItem(
            (string) $schedule_message->id,
            $schedule_message->get_next_execution()
        );

        $this->dispatch_job($queue_item);
    }

    /**
     * @param ScheduleMessageQueueItem $queue_item
     * @param ScheduleMessage $schedule_message
     * @throws Exception
     */
    public function process_job(ScheduleMessageQueueItem $queue_item, ScheduleMessage $schedule_message): void
    {
        $data = [
            'recipients' => ['+23300000000','+23300000000','+23300000000','+23300000000'],
            'sender' => 'test',
            'message' => 'Hello World',
            'schedule_date_time' => $queue_item->get_next_execution()->format('Y-m-d H:i'),
            'processed_ts' => (new DateTime())->format('Y-m-d H:i')
        ];

        $this->message_service->send_message($data);

        if ($schedule_message->will_message_reoccur()) {
            $queue_item->set_next_execution($schedule_message->get_next_execution());

            $this->dispatch_job($queue_item);
        }
    }

    /**
     * @param ScheduleMessageQueueItem $queueItem
     */
    private function dispatch_job(ScheduleMessageQueueItem $queueItem): void
    {
        ProcessScheduledMessage::dispatch($queueItem)
            ->delay($queueItem->get_next_execution());
    }
}
