<?php

namespace App\Services;

use App\Jobs\ProcessMessage;

class MessageService
{
    public function send_message(array $dispatch_data): void
    {
        ProcessMessage::dispatch($dispatch_data);
    }
}
