<?php

namespace App\Console;

use App\Console\Commands\ScheduleMessageQueueJobAudit;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command(ScheduleMessageQueueJobAudit::class)
            ->twiceDaily(0, 12);

        // todo: consider creating another command to gather all failed jobs which were in the schedule-message-queue, log it, then delete the failed jobs. This is because failed jobs should not be manually re-attempted from the failed queue.
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
