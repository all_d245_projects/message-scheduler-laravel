<?php

namespace App\Console\Commands;

use App\Events\QueueScheduleMessage;
use App\Models\ScheduleMessage;
use Illuminate\Console\Command;
use Symfony\Component\Console\Command\Command as CommandAlias;
use Exception;

class ScheduleMessageQueueSimulator extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule_message:simulator';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to simulate creating a schedule message to test the flow.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $type = $this->choice(
                'Select a schedule type?',
                [
                    ScheduleMessage::SCHEDULE_TYPE_SPECIFIC,
                    ScheduleMessage::SCHEDULE_TYPE_DAILY,
                    ScheduleMessage::SCHEDULE_TYPE_WEEKLY,
                    ScheduleMessage::SCHEDULE_TYPE_MONTHLY
                ]
            );

            switch ($type) {
                case ScheduleMessage::SCHEDULE_TYPE_SPECIFIC:
                    $date = $this->ask('Input Schedule Date: eg (YYYY-MM-DD)');

                    $schedule_message = $this->get_schedule_message([
                        'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_SPECIFIC,
                        'is_recurring' => false,
                        'schedule_date' => $date,
                    ]);

                    $schedule_message->save();

                    $this->info('Done. Check jobs table for newly created job IF the schedule date was in the future');

                    break;
                case ScheduleMessage::SCHEDULE_TYPE_DAILY:

                    $this->info('daily schedule type only supports a frequency of 1 which will automatically be applied.');

                    $schedule_message = $this->get_schedule_message([
                        'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_DAILY,
                        'is_recurring' => true,
                        'frequency' => 1,
                    ]);

                    $schedule_message->save();

                    $this->info('Done. Check jobs table for newly created job. After the first job consumption, the same job should be re-inserted for the next day');

                    break;

                case ScheduleMessage::SCHEDULE_TYPE_WEEKLY:

                    $this->info('weekly schedule type only supports a frequency of 1 which will automatically be applied.');

                    $specific_week_days = $this->choice(
                        'Select recurring weekdays',
                        ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sundary'],
                        null,
                        7,
                        true
                    );

                    $schedule_message = $this->get_schedule_message([
                        'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_WEEKLY,
                        'is_recurring' => true,
                        'frequency' => 1,
                        'specific_week_days' => implode(',', $specific_week_days)
                    ]);

                    $schedule_message->save();

                    $this->info('Done. Check jobs table for newly created job. After the first job consumption, the same job should be re-inserted for the next day');

                    break;

                case ScheduleMessage::SCHEDULE_TYPE_MONTHLY:

                    $this->info('monthly schedule type only supports a frequency of 1 which will automatically be applied.');

                    $specific_month_days = $this->ask('Input comma separated month days:',);

                    $schedule_message = $this->get_schedule_message([
                        'schedule_type' => ScheduleMessage::SCHEDULE_TYPE_MONTHLY,
                        'is_recurring' => true,
                        'frequency' => 1,
                        'specific_month_days' => $specific_month_days
                    ]);

                    $schedule_message->save();

                    $this->info('Done. Check jobs table for newly created job. After the first job consumption, the same job should be re-inserted for the next day');

                    break;

                default:
                    throw new Exception('Invalid schedule type');
            }

            return CommandAlias::SUCCESS;
        } catch (Exception $exception) {
            // todo log the exception
            $this->error($exception->getMessage());
            return CommandAlias::FAILURE;
        }
    }

    /**
     * @return array
     */
    private function prompt_for_time(): array
    {
        $hour = $this->ask('Input hour in 24h format with leading 0: eg (05)');
        $minute = $this->ask('Input minute with leading 0: eg (09');

        return [$hour, $minute];
    }

    /**
     * @param array $config
     * @return ScheduleMessage
     */
    private function get_schedule_message(array $config): ScheduleMessage
    {
        list($hour, $minute) = $this->prompt_for_time();

        $default_config = [
            'group_id' => 1,
            'message_id' => 1,
            'hour' => $hour,
            'minute' => $minute
        ];

        $config = array_merge($config, $default_config);

        return new ScheduleMessage($config);
    }

}
