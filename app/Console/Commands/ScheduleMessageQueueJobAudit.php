<?php

namespace App\Console\Commands;

use App\Events\QueueScheduleMessage;
use Illuminate\Console\Command;
use Symfony\Component\Console\Command\Command as CommandAlias;
use Exception;

class ScheduleMessageQueueJobAudit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'schedule_message:audit';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command to audit all active schedule messages to determine if it\'s corresponding queue job has been created';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        try {
            $schedule_messages = $this->get_schedule_messages_with_missing_queue_jobs();

            foreach ($schedule_messages as $schedule_message) {
                QueueScheduleMessage::dispatch($schedule_message);
            }

            return CommandAlias::SUCCESS;
        } catch (Exception $exception) {
            // todo log the exception
            return CommandAlias::FAILURE;
        }
    }

    /**
     * @return iterable
     */
    private function get_schedule_messages_with_missing_queue_jobs(): iterable
    {
        // todo
        return [];
    }
}
