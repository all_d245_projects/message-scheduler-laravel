<?php

namespace App\Listeners;

use App\Events\QueueScheduleMessage;
use App\Services\ScheduledMessageService;
use Exception;

class QueueScheduleMessageListener
{
    /**
     * @var ScheduledMessageService
     */
    private $scheduled_message_service;

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct(ScheduledMessageService $scheduled_message_service)
    {
        $this->scheduled_message_service = $scheduled_message_service;
    }

    /**
     * Handle the event.
     *
     * @param QueueScheduleMessage $event
     * @return void
     * @throws Exception
     */
    public function handle(QueueScheduleMessage $event)
    {
        $schedule_message = $event->schedule_message;

        try {
            if ($schedule_message->will_message_reoccur()) {
                $this->scheduled_message_service->dispatch_scheduled_message_queue_job($schedule_message);
            }
        } catch (Exception $exception) {
            // catching Exception to not halt any process on failure
        }
    }
}
